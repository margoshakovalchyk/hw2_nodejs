/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {addNote, getNotes} = require('../controllers/notesController');
const {getNoteById, updateNoteById, changeCompleted, deleteNoteById} =
require('../controllers/noteController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateNote} = require('./middlewares/validateNote');

router.use(authMiddleware);
router.get('/', asyncWrapper(getNotes));
router.post('/', asyncWrapper(validateNote), asyncWrapper(addNote));

router.get('/:id', asyncWrapper(getNoteById));
router.put('/:id', asyncWrapper(validateNote), asyncWrapper(updateNoteById));
router.patch('/:id', asyncWrapper(changeCompleted));
router.delete('/:id', asyncWrapper(deleteNoteById));

module.exports = router;
