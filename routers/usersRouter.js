/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {getUserInfo, deleteProfile, changePassword} =
require('../controllers/usersController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateChangePassword} =
require('./middlewares/validateChangePassword');

router.use(authMiddleware);
router.get('/me', asyncWrapper(getUserInfo));
router.delete('/me', asyncWrapper(deleteProfile));
router.patch('/me',
    asyncWrapper(validateChangePassword)
    , asyncWrapper(changePassword));

module.exports = router;
