const Joi = require('joi');

module.exports.validateNote = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
