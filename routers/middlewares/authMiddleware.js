require('dotenv').config();
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const {JWTTokenError, HeaderError} = require('../../models/errorModel');

module.exports.authMiddleware = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    throw new HeaderError;
  }

  const [, token] = authHeader.split(' ');

  if (!token) {
    throw new JWTTokenError;
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
