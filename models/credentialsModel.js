/* eslint-disable new-cap */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const credentialSchema = Schema({
  username: {
    type: String,
    required: true,
    uniq: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports.Credential = mongoose.model('Credential', credentialSchema);
