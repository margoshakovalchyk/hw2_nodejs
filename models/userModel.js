/* eslint-disable new-cap */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
  username: {
    type: String,
    required: true,
    uniq: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
