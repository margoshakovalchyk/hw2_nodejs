/* eslint-disable new-cap */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Note = mongoose.model('Note', noteSchema);
