/* eslint-disable require-jsdoc */
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};

module.exports.asyncWrapper = asyncWrapper;
