/* eslint-disable require-jsdoc */
const {
  UnauthorizedError,
  UserExistanceError,
  BadRequestError,
  JWTTokenError,
  HeaderError,
  PasswordMatchError,
} = require('../models/errorModel');

function fail(err, res) {
  const {statusCode, message} = err;

  if (err instanceof UnauthorizedError) {
    return res.status(statusCode).json({message});
  }
  if (err instanceof UserExistanceError) {
    return res.status(statusCode).json({message});
  }
  if (err instanceof BadRequestError) {
    return res.status(statusCode).json({message});
  }
  if (err instanceof JWTTokenError) {
    return res.status(statusCode).json({message});
  }
  if (err instanceof HeaderError) {
    return res.status(statusCode).json({message});
  }
  if (err instanceof PasswordMatchError) {
    return res.status(statusCode).json({message});
  }
  return res.status(500)
      .json({message: message.toString() || 'Server Error'});
}

module.exports.fail = fail;
