const {Note} = require('../models/noteModel');
const {User} = require('../models/userModel');
const {BadRequestError} = require('../models/errorModel');
const {compareIds} = require('./compareIds');

// eslint-disable-next-line require-jsdoc
async function handleNote(id, username) {
  const note = await Note.findById(id);
  const user = await User.findOne({username});
  const userIdx = user._id;

  if (compareIds(note.userId, userIdx)) {
    throw new BadRequestError('Hey, you are not allowed');
  }

  if (!note) {
    throw new BadRequestError(`Note not found`);
  }

  return {note, user};
}

module.exports.handleNote = handleNote;
