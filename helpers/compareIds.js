/* eslint-disable require-jsdoc */
function compareIds(idFirst, idSecond) {
  return JSON.stringify(idFirst) !== JSON.stringify(idSecond);
}

module.exports.compareIds = compareIds;
