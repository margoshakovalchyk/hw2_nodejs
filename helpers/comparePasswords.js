const bcrypt = require('bcrypt');
const {PasswordMatchError} = require('../models/errorModel');

const comparePasswords = async (firstPassword, secondPassword) => {
  if (!await bcrypt.compare(firstPassword, secondPassword)) {
    throw new PasswordMatchError;
  }
};

module.exports.comparePasswords = comparePasswords;
