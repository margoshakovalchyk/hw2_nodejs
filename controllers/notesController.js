const mongoose = require('mongoose');
const {Note} = require('../models/noteModel');
const {User} = require('../models/userModel');
const {UserExistanceError} = require('../models/errorModel');

module.exports.addNote = async (req, res) => {
  const {text} = req.body;
  const {username} = req.user;

  const userCreated = await User.findOne({username});

  const note = new Note({
    text,
    // eslint-disable-next-line new-cap
    userId: mongoose.Types.ObjectId(userCreated._id),
  });

  await note.save();

  res.json({message: 'Success'});
};


module.exports.getNotes = async (req, res) => {
  const {username} = req.user;
  const {offset = 0, limit = 5} = req.query;
  const user = await User.findOne({username});

  if (!user) {
    throw new UserExistanceError();
  }

  const notes = await Note.find(
      {userId: user._id},
      [],
      {
        offset: parseInt(offset),
        limit: limit > 100 ? 5 : parseInt(limit),
      },
  );

  res.json({notes});
};
