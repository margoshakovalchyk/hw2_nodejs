const {Note} = require('../models/noteModel');
const {handleNote} = require('../helpers/noteHandler');

module.exports.getNoteById = async (req, res) => {
  const {id} = req.params;
  const {username} = req.user;

  const {note} = await handleNote(id, username);

  res.json({'note': note});
};

module.exports.updateNoteById = async (req, res) => {
  const {id} = req.params;
  const {username} = req.user;
  const {text} = req.body;

  await handleNote(id, username);

  await Note.updateOne({_id: id}, {$set: {'text': text}});
  res.json({message: 'Success'});
};

module.exports.changeCompleted = async (req, res) => {
  const {id} = req.params;
  const {username} = req.user;

  const {note} = await handleNote(id, username);

  let check = note.completed;
  check = !check;

  await Note.updateOne({_id: id}, {$set: {'completed': check}});
  res.json({message: 'Success'});
};

module.exports.deleteNoteById = async (req, res) => {
  const {id} = req.params;
  const {username} = req.user;

  await handleNote(id, username);

  await Note.deleteOne({_id: id});
  res.json({message: 'Success'});
};
