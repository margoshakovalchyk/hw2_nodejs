const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialsModel');
const {Note} = require('../models/noteModel');
const {comparePasswords} = require('../helpers/comparePasswords');
const {UserExistanceError} = require('../models/errorModel');

module.exports.getUserInfo = async (req, res) => {
  const {username} = req.user;

  const userData = await User.findOne({username});

  if (!userData) {
    throw new UserExistanceError();
  }

  const user = {
    user: userData,
  };

  res.json(user);
};

module.exports.deleteProfile = async (req, res) => {
  const {username} = req.user;

  const userData = await User.findOne({username});

  if (!userData) {
    throw new UserExistanceError();
  }

  await User.deleteOne({username});
  await Credential.deleteOne({username});
  await Note.deleteMany({userId: userData._id});

  res.json({'message': 'Profile was successfully deleted'});
};


module.exports.changePassword = async (req, res) => {
  const {username} = req.user;
  const {oldPassword, newPassword} = req.body;

  const userCredential = await Credential.findOne({username});

  await comparePasswords(oldPassword + '', userCredential.password);

  const password = await bcrypt.hash(newPassword, 10);

  await Credential.updateOne({username}, {$set: {'password': password}});

  res.json({'message': 'Password was successfully changed'});
};

