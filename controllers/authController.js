require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialsModel');
const {comparePasswords} = require('../helpers/comparePasswords');
const {UserExistanceError} = require('../models/errorModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  const userCreated = await User.findOne({username});

  if (userCreated) {
    throw new UserExistanceError(`User with username already created`);
  }

  const credential = new Credential({
    username,
    password: await bcrypt.hash(password, 10),
  });

  const user = new User({username});

  await credential.save();
  await user.save();

  res.json({message: 'Success'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await Credential.findOne({username});

  if (!user) {
    throw new UserExistanceError();
  }

  await comparePasswords(password + '', user.password);


  const jwtToken = jwt.sign({
    username: user.username,
    __id: user.__id,
  }, JWT_SECRET);

  res.json({'message': 'Success', 'jwt_token': jwtToken});
};
